#!/bin/bash
cd $(dirname $0)
cd ..

qmake Qt5HttpServer.pro
make

mkdir -p include
cd src
cp -v --parents `find -name \*.h -not -name *_p.h` ../include/
