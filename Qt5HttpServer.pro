QT -= gui
QT += core network
CONFIG += c++11
TARGET = qt5httpserver
TEMPLATE = lib

DEFINES += QT5HTTPSERVER

INCLUDEPATH += $$PWD/src

SOURCES += \
    src/QtHttpClientWrapper.cpp \
    src/QtHttpHeader.cpp \
    src/QtHttpReply.cpp \
    src/QtHttpRequest.cpp \
    src/QtHttpServer.cpp


HEADERS += \
    src/QtHttpClientWrapper.h \
    src/QtHttpHeader.h \
    src/QtHttpReply.h \
    src/QtHttpRequest.h \
    src/QtHttpServer.h \
    src/qt5httpserver_global.h

unix: TMP_DIR = .tmp
win32: TMP_DIR = tmp


CONFIG(debug, debug|release) {
    TARGET = qt5httpserver_debug
    DESTDIR = $$PWD/debug
    OBJECTS_DIR = $$PWD/$$TMP_DIR/debug
    MOC_DIR = $$PWD/$$TMP_DIR/debug
    RCC_DIR = $$PWD/$$TMP_DIR/debug
    UI_DIR = $$PWD/$$TMP_DIR/debug
}
else {
    DESTDIR = $$PWD/release
    OBJECTS_DIR = $$PWD/$$TMP_DIR/release
    MOC_DIR = $$PWD/$$TMP_DIR/release
    RCC_DIR = $$PWD/$$TMP_DIR/release
    UI_DIR = $$PWD/$$TMP_DIR/release
}
